// Al cargarse el documento
window.onload = function () {
  init_scroll();
  init_gallery();
  init_map();
}

// Modifica el comportamiento del scroll
function init_scroll () {
  let btns = document.getElementsByClassName('nav-btn');
  for (var i = 0; i < btns.length; i++) {
    nav_go(btns[i]);
  }
}

// Modifica el comportamiento de los botones de navegación
function nav_go (btn) {
  btn.addEventListener('click', function (e) {
    let a = e.target.nodeName == 'A' ? e.target : e.target.parentNode,
        id = a.href.split('#')[a.href.split('#').length - 1],
        sec = document.getElementById(id),
        offset = 70,
        position = sec.getBoundingClientRect().top + window.pageYOffset - offset;
    e.preventDefault();
    window.scrollTo({ top: position, behavior: "smooth" });
  });
}

// Agrega funcionalidades para la galería
function init_gallery () {
  lightGallery(document.getElementById('gallery'), {
    plugins: [lgZoom, lgThumbnail],
    thumbnail: true,
  });
}

// Añade el mapa
function init_map () {
  const coords = [19.24554, -103.74161],
    address = "<b>Agroteso Colima</b><br/><a href='tel:3123070347'>+52 312 307 0347</a><br/>Río Armería 371-A<br/>Placetas Estadio<br/>28050, Colima<br/><a href='mailto:contacto@agroteso.com'>contacto@agroteso.com</a>";
  var map = L.map('map').setView(coords, 16),
      icon = L.icon({ iconUrl: './assets/favicon.png', iconSize: [30, 30] }),
      marker = L.marker(coords, { icon: icon }).addTo(map);
  marker.bindPopup(address).openPopup();
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 20,
  }).addTo(map);
}
